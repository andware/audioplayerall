#万能视频解码播放器
##1.vitamio为解码器的 android 库
##2.vitamio-sample为解码器的实现示例
##3.VideoTest是根据第二个做的可以通过 videoview 观看网络连接视频的 Demo
###注意：
	使用时必须在 AndroidManifest 中注册
	···
	 <activity
            android:name="io.vov.vitamio.activity.InitActivity"
            android:configChanges="orientation|keyboardHidden|navigation"
            android:launchMode="singleTop"
            android:theme="@android:style/Theme.NoTitleBar"
            android:windowSoftInputMode="stateAlwaysHidden" />
	···

	在布局文件中控件声明为

	···
	<io.vov.vitamio.widget.CenterLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical" >

        <io.vov.vitamio.widget.VideoView
            android:id="@+id/surface_view"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_centerHorizontal="true"
            android:layout_centerVertical="true" />
    </io.vov.vitamio.widget.CenterLayout>
	···