package com.example.videotest;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText myWebSite;
	private Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		myWebSite = (EditText) findViewById(R.id.editText1);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(listener);
		
	}
	
	private OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			String mySite = myWebSite.getText().toString();
			if ( !mySite.equals("") ) {
				if ( mySite.contains(".mkv")  ) {

					Intent intent = new Intent();
					intent.setClass(getApplicationContext(), PlayVideoActivity.class);
					intent.putExtra("myWebSite", myWebSite.getText().toString());
					Log.i("TestVideo",myWebSite.getText().toString());
					startActivity(intent);
					MainActivity.this.finish();
				} else {
					new AlertDialog.Builder(MainActivity.this).setTitle("Dialog").setMessage("Can't play this video!").setPositiveButton("Sure", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// TODO Auto-generated method stub
							
						}
					}).show();
				}
			} else {
				Toast.makeText(getApplicationContext(), "Please edit VideoViewDemo Activity, and set path" + " variable to your media file URL/path", Toast.LENGTH_LONG).show();
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
