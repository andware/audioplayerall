package com.example.videotest;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class PlayVideoActivity extends Activity{


	private VideoView mVideoView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (!LibsChecker.checkVitamioLibs(this))
			return;
		setContentView(R.layout.play_video);

		mVideoView = (VideoView) findViewById(R.id.surface_view);
		Intent intent = getIntent();
		mVideoView.setVideoPath(intent.getStringExtra("myWebSite"));
		mVideoView.setMediaController(new MediaController(getApplicationContext()));
		mVideoView.requestFocus();
		Log.i("TestVideo",mVideoView.getResources().toString());
		mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mediaPlayer) {
				// optional need Vitamio 4.0
				mediaPlayer.setPlaybackSpeed(1.0f);
			}
		});
	}
	
}
